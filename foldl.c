#include "header.h"
#include "linked.h"

int foldl( struct linked_list* list, int accumulator, int (*apply)(int, int) ){
	struct node* current_node;
	int result = accumulator;

	if ( list->size <= 0 ){
		return -1;
	}

	current_node = list->head;
	result = apply( result, current_node->value );

	while( current_node->next != NULL ) {
		current_node = current_node->next;
		result = apply( result, current_node->value );
	}

	return result;
}
