#include "header.h"
#include "linked.h"

struct linked_list* map_mut( struct linked_list* list, int (*apply)(int) ){
	struct linked_list* new_list;
	struct node* current_node;

	if ( list->size <= 0 ){
		return NULL;
	}

	current_node = list->head;
	current_node->value = apply( current_node->value );
	new_list = list_create( current_node->value );

	while( current_node->next != NULL ) {
		current_node = current_node->next;
		current_node->value = apply( current_node->value );
		list_add_back( current_node->value, &new_list );
	}

	return new_list;
}
