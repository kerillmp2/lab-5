#include "header.h"

struct node {
	int value;
	struct node* next;
};

struct linked_list {
	struct node* head;
	struct node* tail;
	int size;
	int sum;
};

struct linked_list* list_create( int value ) {
	struct node* head = ( struct node* ) malloc( sizeof( struct node ) );
	struct linked_list* list = ( struct linked_list* ) malloc( sizeof( struct linked_list ) );

	head->value = value;
	head->next = NULL;

	list->head = head;
	list->tail = head;
	list->sum = value;
	list->size = 1;

	return list;
}

void list_add_front( int value, struct linked_list** list ) {
	struct node* new_head = ( struct node* ) malloc( sizeof( struct node ) );
	struct linked_list* llist = *list;

	new_head->value = value;
	new_head->next = llist->head;
	llist->head = new_head;
	llist->size += 1;
	llist->sum += value;

	return;
}

void list_add_back( int value, struct linked_list** list ) {
	struct node* new_tail = ( struct node* ) malloc( sizeof( struct node ) );
	struct linked_list* llist = *list;

	new_tail->value = value;
	new_tail->next = NULL;

	if ( llist->size == 0 ) {
		llist->head = new_tail;
		llist->tail = new_tail;
		llist->size += 1;
		llist->sum += value;
	} else {
		llist->tail->next = new_tail;
		llist->tail = new_tail;
		llist->size += 1;
		llist->sum += value;
	}
	return;
}

struct node* list_node_at( int index, struct linked_list** list ) {
	int i;
	struct linked_list* llist = *list;
	struct node* current_node = llist->head;

	if ( index > ( llist->size - 1 ) ) {
		return NULL;
	}

	for ( i = 0; i < index; i++ ) {
		current_node = current_node->next;
	}

	return current_node;
}

int list_get( int index, struct linked_list** list ) {
	struct node* node_at_index = list_node_at( index, list );

	if ( node_at_index == NULL ) {
		return 0;
	}

	return node_at_index->value;
}

int list_length( struct linked_list** list ) {
	struct linked_list* llist = *list;
	return llist->size;
}

int list_sum( struct linked_list** list ) {
	struct linked_list* llist = *list;
	return llist->sum;
}

void list_delete_first ( struct linked_list** list ) {
	struct linked_list* llist = *list;
	struct node* head_node = llist->head;
	if ( llist->size <= 0 ) {
		return;
	}

	llist->head = head_node->next;
	llist->sum -= head_node->value;
	llist->size -= 1;
	free( head_node );
	return;
}

void list_free ( struct linked_list** list ) {
	struct linked_list* llist = *list;
	while ( llist->size > 0 ) {
		list_delete_first ( list );
	}
	free ( llist );
	return;
}
