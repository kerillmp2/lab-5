#include "header.h"
#include "linked.h"

bool deserialize( struct linked_list** list, const char* filename ) {
	FILE* file;
	int value;
	int i;
	bool flag = true;
	struct linked_list* f_list = list_create( 1 );
	list_delete_first( &f_list );

	file = fopen( filename, "r" );

	while( flag ) {
		flag = fread( &value, sizeof(int), 1, file );
		if( ferror( file ) ) {
			flag = false;
			break;
		}
		if( feof( file ) ) {
			flag = true;
			break;
		}
		list_add_back( value, &f_list );
	}

	fclose( file );

	if ( flag ) {
		for ( i = 0; i < f_list->size; i++ ) {
			list_add_back( list_get( i, &f_list ), list) ;
		}
	}

	return flag;
}
