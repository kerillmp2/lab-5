#include "header.h"
#include "linked.h"

bool save( struct linked_list* list, const char* filename ) {
	FILE* file;
	int i;
	bool flag = true;

	file = fopen( filename, "w" );

	if ( list->size <= 0 ) {
		flag = ( fprintf( file, " " ) < 0 ? false : true );
		fclose( file );
		return flag;
	}

	for( i = 0; i < list->size; i++ ) {
		if( !fprintf( file, "%d ", list_get( i, &list ) ) ) {
			fclose( file );
			return false;
		}
	}

	fclose( file );
	return true;
}
