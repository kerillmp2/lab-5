#include "header.h"
#include "linked.h"

struct linked_list* map( struct linked_list* list, int (*apply)(int) ){
	struct linked_list* new_list;
	struct node* current_node;
	int new_list_value;

	if ( list->size <= 0 ){
		return NULL;
	}

	current_node = list->head;
	new_list_value = current_node->value;
	new_list_value = apply( new_list_value );
	new_list = list_create( new_list_value );

	while( current_node->next != NULL ) {
		current_node = current_node->next;
		new_list_value = current_node->value;
		new_list_value = apply( new_list_value );
		list_add_back( new_list_value, &new_list );
	}

	return new_list;
}
