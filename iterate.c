#include "header.h"
#include "linked.h"

struct linked_list* iterate( int initial, int size, int (*apply)(int) ){
	struct linked_list* new_list;
	int current_value = initial;
	int i;

	new_list = list_create( current_value );

	for( i = 1; i < size; i++ ) {
		current_value = apply( current_value );
		list_add_back( current_value, &new_list );
	}

	return new_list;
}
