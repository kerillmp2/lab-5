#include "header.h"
#include "linked.h"

void foreach( struct linked_list* list, void (*apply)(int) ){
	struct node* current_node = list->head;
	if ( list->size <= 0 ) {
		return;
	}

	apply( current_node->value );

	while(  current_node->next != NULL ){
		current_node = current_node->next;
		apply( current_node->value );
	}
}
