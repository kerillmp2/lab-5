#include "header.h"
#include "linked.h"

bool serialize( struct linked_list* list, const char* filename ) {
	FILE* file;
	int i;
	int value;
	bool flag = true;

	file = fopen( filename, "w" );

	if ( list->size <= 0 ) {
		flag = ( fprintf( file, " " ) < 0 ? false : true );
		fclose( file );
		return flag;
	}

	for( i = 0; i < list->size; i++ ) {
		value = list_get( i, &list );
		if( !fwrite( &value, sizeof(int), 1, file ) ) {
			fclose( file );
			return false;
		}
	}

	fclose( file );
	return true;
}
