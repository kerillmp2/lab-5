#include "header.h"
#include "linked.h"
#include "foreach.h"
#include "map.h"
#include "map_mut.h"
#include "foldl.h"
#include "iterate.h"
#include "save.h"
#include "load.h"
#include "serialize.h"
#include "deserialize.h"

struct linked_list* read_list_from_input( void ) {
	int value;
	struct linked_list* list = ( struct linked_list* ) malloc( sizeof( struct linked_list ) );

	list->size = 0;

	puts("Enter numbers...");
	while( scanf( "%d", &value ) != EOF ){
		if ( list->size == 0 ){
			list = list_create( value );
		}else{
			list_add_front( value, &list );
		}
	}

	return list;
}

void print_int_space( int value ){
	printf("%d ", value);
}

void print_list_space( struct linked_list* list ) {
	printf("Elements, separeted by spaces:\n");
	foreach( list, &print_int_space );
	printf("\n");
}

void print_int_endline( int value ) {
	printf("%d\n", value);
}

void print_list_endline( struct linked_list* list ) {
	printf("Elements, separeted by endline:\n");
	foreach( list, &print_int_endline );
	printf("\n");
}


int print_square( int a ){
	int square = a * a;
	printf( "%d ", square );
	return square;
}

void print_list_square( struct linked_list* list ) {
	printf("Squared elements:\n");
	map( list, &print_square );
	printf("\n");
}

int print_cube( int a ){
	int cube = a * a * a;
	printf( "%d ", cube );
	return cube;
}

void print_list_cube( struct linked_list* list ) {
	printf("Cubed elements:\n");
	map( list, &print_cube );
	printf("\n");
}

int sum( int a, int b ){
	return ( a + b );
}

void print_sum( struct linked_list* list ) {
	printf( "Sum by foldl: %d\n", foldl( list, 0, &sum ) );
}

int min (int a, int b ) {
	return ( a < b ? a : b );
}

void print_min( struct linked_list* list ) {
	printf( "Min: %d\n", foldl( list, INT_MAX, &min ) );
}

int max (int a, int b ) {
	return ( a > b ? a : b );
}

void print_max( struct linked_list* list ) {
	printf( "Max: %d\n", foldl( list, INT_MIN, &max ) );
}

int module( int a ) {
	return ( a < 0 ? (-1)*a : a );
}

void module_list( struct linked_list* list ) {
	map_mut( list, &module );
}

int mult_two( int a ){
	return ( a * 2 );
}

void save_list( struct linked_list* list, const char* filename, bool (*my_save)(struct linked_list*, const char*) ) {
	if ( my_save( list, filename ) ) {
		printf( "List saved into file %s\n", filename );
	} else {
		printf( "Error on saving list into file %s\n", filename );
	}
}

void load_list( struct linked_list* list, const char* filename, bool (*my_load)(struct linked_list**, const char*) ) {
	if ( my_load( &list, filename ) ) {
		printf( "List loaded from file %s\n", filename );
	} else {
		printf( "Error on load list from file %s\n", filename );
	}
}


int main( void ) {
	struct linked_list* list = read_list_from_input();
	struct linked_list* powers = iterate( 1, 10, &mult_two );
	struct linked_list* sl = list_create( 0 );

	list_delete_first( &sl );

	print_list_space( list );
	print_list_endline( list );

	print_list_square( list );
	print_list_cube( list );

	print_sum( list );
	print_min( list );
	print_max( list );

	module_list( list );
	print_list_space( list );

	print_list_space( powers );

	save_list( list, "my_list.txt", &save );
	load_list( sl, "my_list.txt", &load );
	print_list_space( sl );

	save_list( list, "my_list.txt", &serialize );
	load_list( sl, "my_list.txt", &deserialize );
	print_list_space( sl );

	list_free( &list );
	list_free( &powers );
	list_free( &sl );
	return 0;
}
